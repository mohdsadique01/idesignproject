import React from 'react'
import "./Product.css"

function Product({product}) {
 

  const handleclick = (e) => {
console.log(e,"ggg",product)
   if(product.stock<50){
    alert('Hurry up stock is limited');
   }
  
};
  return (
    
     <div className='main-box'onClick={(product)=>handleclick(product)} >
        <div className='img-cont'>
            {

            <img src={product.images[0]}
            alt='product'/>
            }
        </div>
        <div className='title'><span>{product.title}</span></div>
        <div className='price'>$<span>{product.price}</span></div>
        <div className='subtitle' >DiscountPercentage:<span style={{fontSize:'x-large'}}>{product.discountPercentage}%</span></div>
        <div className='subtitle'>Ratng:<span>{product.rating}</span></div>
        <div className='subtitle'>Brand:<span>{product.brand}</span></div>
        <div ><button className='btn'>Add to cart</button></div>
    </div>
   
  )
}

export default Product
