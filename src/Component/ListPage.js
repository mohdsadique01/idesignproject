import React,{ useEffect, useState }  from 'react'
import Product from './Product';
import Nav from './Nav';
import "./ListPage.css"
function ListPage() {
    let [isLoading,setLoading]=useState(true)

    let[data,setData]=useState([])

    let productsState={
      products:[],
     
    }
    let [displayData,setProducts]=useState(productsState)

    useEffect(()=>{
      fetch('https://dummyjson.com/products?limit=100')
      .then(res=>res.json())
      .then(data=>{
        setData(data.products)
       console.log();
       let products= data.products
       setProducts({
        products
       
        
      })
    })
    

    },[]);
    console.log(data);

  
   let sortProductBySouce=(source)=>{
    console.log(source,'sourceeee',products  )
    let sortedProducts
    if(source === 'discountPercentage'){
      
       sortedProducts = products.sort((a,b) => a.discountPercentage > b.discountPercentage? 1 : -1);
    } else if(source == 'price') {
       sortedProducts  = products.sort((a,b) => a.price > b.price? 1 : -1)
    }else if(source == 'rating') {
       sortedProducts  = products.sort((a,b) => a.rating > b.rating? 1 : -1)
    }
    setProducts({products:sortedProducts})
    console.log(sortedProducts,'sorted product');
    }

const handleClick=(e)=>{
    let v=e.target.value;
    console.log(v);
    setProducts({
      ...displayData
   
    })
  }
  let {products}=displayData
 
  let[search,setSearch]=useState('');
  
  const handleChange=(e)=>{
    let v=e.target.value
    setSearch(v)
    console.log(v);
    filterSearch(v)

  }

    
    const catogries={}
    data.forEach(product=>{
    catogries[product['category']]=1
    })
    console.log(catogries, 'catogries')


    let [category,setCategory]=useState('all')
    useEffect(()=>{
      filterSearch("")
  },[category])
    const handleSelect=(e)=>{
      console.log(e.target.value,'..handlesele');
      setCategory(e.target.value)
      
  
    }
    console.log(category , 'dddddd');


    const filterSearch=(input)=>{
        let filterd=[]
        if(category==='all'){
    
           filterd=data.filter((product)=>product.brand.toLowerCase().includes(input.toLowerCase()))
        }
        else{
           filterd=data.filter((product)=>product.brand.toLowerCase().includes(input.toLowerCase()) && product.category===category)
        }
        console.log(filterd,"rrrrrr");
        setProducts({
            ...displayData,
           products:filterd,
            
          })
        }
       
  return (
    <div>
      <div><Nav /></div>
      <div className='button' ><button  className='bttn' onClick={()=> (sortProductBySouce('discountPercentage'))}>short by discount</button> <button className='bttn' onClick={()=> sortProductBySouce('price')}>short by rating</button> <button  className='bttn' onClick={()=> sortProductBySouce('rating')}>short by price</button></div>
      <div className='search-box'>
      <input value={search} onChange={handleChange} type="text" placeholder='search for brand'></input>
      <select onChange={(e)=>handleSelect(e)}>
        <option value='all'>All catogries</option>
        {Object.keys(catogries).map((category,idx)=>(
          <option value={category} key={idx}>{category}</option>
        ))}
      </select>
    </div>
    <div className='box'>
      
      {products.map((product,idx)=>(
        <div >
        <Product product={product} key={idx}/>
        </div>
      ))}
       </div>
    </div>
  )
}

export default ListPage
