
import './App.css';
import ListPage from './Component/ListPage';

function App() {
  return (
    <div className="App">
     <ListPage />
    </div>
  );
}

export default App;
